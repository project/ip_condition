# IP Condition

Provides condition plugin for the current visitors IP address. Compatible with block visibility and any other
component utilizing condition plugins.

## Usage

1. Download and install the `drupal/ip_condition` module. Recommended install method is composer:
   ```
   composer require drupal/ip_condition
   ```
2. Enter the desired IPs in the "IP Addresses" vertical tab.
3. Review additional configurations and save changes.
