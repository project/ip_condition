<?php

namespace Drupal\ip_condition\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides the current visitors IP address condition plugin.
 *
 * @Condition(
 *   id = "ip_condition",
 *   label = @Translation("IP Address"),
 * )
 */
class IpCondition extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * IpCondition constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(RequestStack $requestStack, array $configuration, string $plugin_id, array $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('request_stack'),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + [
      'ips' => NULL,
      'untrusted' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['ips'] = [
      '#type' => 'textarea',
      '#title' => $this->t('IP Addresses'),
      '#description' => $this->t("Enter one IP address per line. The '*' character is a wildcard. Examples: <pre>256.1.65.4<br />256.1.65.*<br />256.*.65.5</pre>"),
      '#element_validate' => [[$this, 'validateIps']],
      '#default_value' => $this->configuration['ips'],
    ];
    $form['untrusted'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Check untrusted IP addresses'),
      '#description' => $this->t('When enabled both trusted and untrusted client IP addresses are checked. <strong>Not recommended.</strong>'),
      '#default_value' => $this->configuration['untrusted'],
    ];
    return $form;
  }

  /**
   * Validate callback for IP address value.
   */
  public function validateIps(array &$element, FormStateInterface $form_state): void {
    if (preg_match('/[^\W\d.*]/i', $element['#value'])) {
      $form_state->setError($element, $this->t('Invalid characters detected. IP addresses may include numbers and decimals.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['ips'] = preg_replace(
      '/\*+/',
      '*',
      implode(PHP_EOL, array_map('trim', array_filter(preg_split('/\r\n?|\n/', $form_state->getValue('ips')))))
    );
    $this->configuration['untrusted'] = $form_state->getValue('untrusted');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(): bool {
    if (!$this->configuration['ips']) {
      return TRUE;
    }
    $ips = [$this->requestStack->getCurrentRequest()->getClientIp()];
    if ($this->configuration['untrusted']) {
      $ips = array_merge($ips, $this->requestStack->getCurrentRequest()->getClientIps());
    }

    $patterns = $this->configuration['ips'];
    $replace = [
      // Replace newlines with a logical 'or'.
      '/(\r\n?|\n)/',
      // Quote asterisks.
      '/\\\\\*/',
    ];
    $pattern = '/^(' . preg_replace($replace, ['|', '.*'], preg_quote($patterns, '/')) . ')$/';

    foreach ($ips as $ip) {
      if (preg_match($pattern, $ip)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    if ($this->isNegated()) {
      return $this->t("The current visitors IP address isn't in the whitelist");
    }
    return $this->t('The current visitors IP address is in the whitelist');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    return array_merge(parent::getCacheContexts(), ['user']);
  }

}
